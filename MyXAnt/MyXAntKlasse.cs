using System.Collections.Generic;
using AntMe.Deutsch;

namespace AntMe.Player.MyXAnt
{

	[Spieler(
        Volkname = "XAnts",		 // Hier kannst du den Namen des Volkes festlegen
        Vorname = "Matthias",       // An dieser Stelle kannst du dich als Schöpfer der Ameise eintragen
        Nachname = "Drescher"       // An dieser Stelle kannst du dich als Schöpfer der Ameise eintragen
    )]

    /// Kasten stellen "Berufsgruppen" innerhalb deines Ameisenvolkes dar. Du kannst hier mit
    /// den Fähigkeiten einzelner Ameisen arbeiten. Wie genau das funktioniert kannst du der 
    /// Lektion zur Spezialisierung von Ameisen entnehmen (http://wiki.antme.net/de/Lektion7).
    [Kaste(
        Name = "Standard",                  // Name der Berufsgruppe
        AngriffModifikator = 0,             // Angriffsstärke einer Ameise
        DrehgeschwindigkeitModifikator = 0, // Drehgeschwindigkeit einer Ameise
        EnergieModifikator = 0,             // Lebensenergie einer Ameise
        GeschwindigkeitModifikator = 0,     // Laufgeschwindigkeit einer Ameise
        LastModifikator = 0,                // Tragkraft einer Ameise
        ReichweiteModifikator = 0,          // Ausdauer einer Ameise
        SichtweiteModifikator = 0           // Sichtweite einer Ameise
    )]
    public class MyXAntKlasse : Basisameise
    {
        #region Kasten

        /// <summary>
        /// Jedes mal, wenn eine neue Ameise geboren wird, muss ihre Berufsgruppe
        /// bestimmt werden. Das kannst du mit Hilfe dieses Rückgabewertes dieser 
        /// Methode steuern.
        /// Weitere Infos unter http://wiki.antme.net/de/API1:BestimmeKaste
        /// </summary>
        /// <param name="anzahl">Anzahl Ameisen pro Kaste</param>
        /// <returns>Name der Kaste zu der die geborene Ameise gehören soll</returns>
        public override string BestimmeKaste(Dictionary<string, int> anzahl)
        {
            // Gibt den Namen der betroffenen Kaste zurück.
            return "Standard";
        }

        #endregion

        #region Fortbewegung

       
        public override void Wartet()
        {
			GeheGeradeaus();
        }

        
        public override void WirdMüde()
        {
			GeheZuBau();
        }

       
        public override void IstGestorben(Todesart todesart)
        {
        }

       
        public override void Tick()
        {
	        if (Ziel is Bau && AktuelleLast > 0 && GetragenesObst == null)
	        {
				SprüheMarkierung(Richtung + 180);   
	        }
        }

        #endregion

        #region Nahrung

      
        public override void Sieht(Obst obst)
        {
	        if (!(Ziel is Wanze))
	        {
		        if (AktuelleLast == 0 && BrauchtNochTräger(obst))
		        {
			        GeheZuZiel(obst);
		        }
	        }
        }

        
        public override void Sieht(Zucker zucker)
        {
	        if (!(Ziel is Wanze))
	        {
		        if (AktuelleLast == 0)
		        {
			        GeheZuZiel(zucker);
					SprüheMarkierung(5,100);
		        }
	        }
        }

        
        public override void ZielErreicht(Obst obst)
        {
	        if (BrauchtNochTräger(obst))
	        {
		        Nimm(obst);
		        GeheZuBau();
	        }
        }

        
        public override void ZielErreicht(Zucker zucker)
        {
			Nimm(zucker);
			GeheZuBau();
        }

        #endregion

        #region Kommunikation

      
        public override void RiechtFreund(Markierung markierung)
        {
	        if (Ziel == null)
	        {
				DreheInRichtung(markierung.Information);
		        GeheGeradeaus();
	        }
        }

        /// <summary>
        /// So wie Ameisen unterschiedliche Nahrungsmittel erspähen können, entdecken Sie auch 
        /// andere Spielelemente. Entdeckt die Ameise eine Ameise aus dem eigenen Volk, so 
        /// wird diese Methode aufgerufen.
        /// Weitere Infos unter "http://wiki.antme.net/de/API1:SiehtFreund(Ameise)"
        /// </summary>
        /// <param name="ameise">Erspähte befreundete Ameise</param>
        public override void SiehtFreund(Ameise ameise)
        {
			
        }

        /// <summary>
        /// So wie Ameisen unterschiedliche Nahrungsmittel erspähen können, entdecken Sie auch 
        /// andere Spielelemente. Entdeckt die Ameise eine Ameise aus einem befreundeten Volk 
        /// (Völker im selben Team), so wird diese Methode aufgerufen.
        /// Weitere Infos unter "http://wiki.antme.net/de/API1:SiehtVerb%C3%BCndeten(Ameise)"
        /// </summary>
        /// <param name="ameise">Erspähte verbündete Ameise</param>
        public override void SiehtVerbündeten(Ameise ameise)
        {
        }

        #endregion

        #region Kampf

       
        public override void SiehtFeind(Ameise ameise)
        {
			
        }

        
        public override void SiehtFeind(Wanze wanze)
        {
			LasseNahrungFallen();

	        if (AnzahlAmeisenInSichtweite >= 2)
	        {
		        GreifeAn(wanze);
	        }
	        else
	        {
				GeheWegVon(wanze);   
	        }

        }

       
        public override void WirdAngegriffen(Ameise ameise)
        {
        }

       
        public override void WirdAngegriffen(Wanze wanze)
        {
	        if (AktuelleEnergie < MaximaleEnergie/2)
	        {
		        GeheZuBau();
	        }
        }

        #endregion
    }
}
